output "autoscaling_fargate_lb_domain" {
  value = module.autoscaling_ecs_alb_module.alb.dns_name
}