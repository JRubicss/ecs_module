module "vpc_module" {
  source                   = "../modules/vpc"
  vpc_cidr_block           = var.vpc_cidr_block
  availability_zones       = var.availability_zones
  public_ip_cidrs          = var.public_ip_cidrs
  private_ip_cidrs         = var.private_ip_cidrs
  vpc_name                 = var.vpc_name
  vpc_enable_dns_hostnames = true
}

module "autoscaling_ecs_alb_module" {
  source                   = "../modules/alb"
  vpc_id                   = module.vpc_module.vpc.id
  application_port         = 5000
  http_cidr_block          = ["0.0.0.0/0"]
  ec2_ids                  = []
  subnet_ids               = module.vpc_module.public_subnets.*.id
  alb_name                 = var.alb_name
  target_group_name_prefix = var.target_group_name_prefix
  target_type              = "ip"
}

module "autoscaling_ecs_service" {
  source           = "../modules/autoscaling_ecs_service_infrastructure"
  ecs_cluster_name = var.ecs_cluster_name
  ecs_service_name = module.ecs_service.ecs_service.name
  min_capacity     = var.ecs_min_capacity
  max_capacity     = var.ecs_max_capacity
  ecs_policy_configurations_predefined = [
    {
      predefined_metric_type = "ECSServiceAverageCPUUtilization" #ECSServiceAverageMemoryUtilization
      target_value           = 80
      scale_in_cooldown      = 100
      scale_out_cooldown     = 100
    }
  ]
}

module "ecs_service" {
  source                       = "../modules/ecs_service"
  container_name               = var.container_name
  docker_image                 = var.docker_image
  application_port             = 5000
  ecs_cluster_id               = module.autoscaling_ecs_service.ecs_cluster.id
  requires_compatibilities     = ["FARGATE"]
  service_launch_type          = "FARGATE"
  network_mode                 = "awsvpc"
  network_configuration_needed = true
  subnets                      = module.vpc_module.private_subnets.*.id
  alb_target_group_arn         = module.autoscaling_ecs_alb_module.target_group.arn
  assign_public_ip             = false
  load_balancer_needed         = true
  vpc_id                       = module.vpc_module.vpc.id
  ingress_rules = [
    {
      from_port       = 5000
      to_port         = 5000
      security_groups = [module.autoscaling_ecs_alb_module.alb_security_group.id]
      cidr_blocks     = []
      protocol        = "tcp"
      description     = ""
    }
  ]
  egress_rules = [
    {
      from_port       = 0
      to_port         = 65535
      security_groups = []
      cidr_blocks     = ["0.0.0.0/0"]
      protocol        = "tcp"
      description     = ""
    },
  ]
}
