# vpc

variable "vpc_cidr_block" {
  type    = string
}

variable "availability_zones" {
  type    = list(string)
  default = []
}

variable "public_ip_cidrs" {
  type    = list(string)
  default = []
}

variable "private_ip_cidrs" {
  type    = list(string)
  default = []
}

variable "vpc_name" {
  type    = string
}

# alb

variable "alb_name" {
  type    = string
}

variable "target_group_name_prefix" {
  type    = string
}

# autoscaling_ecs_service_infrastructure

variable "ecs_cluster_name" {
  type    = string
}

variable "ecs_min_capacity" {
  type    = number
}

variable "ecs_max_capacity" {
  type    = number
}

# ecs_service

variable "container_name" {
  type    = string
}

variable "docker_image" {
  type    = string
}
