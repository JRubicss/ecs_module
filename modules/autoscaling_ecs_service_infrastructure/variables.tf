variable "ecs_cluster_name" {
  type    = string
  default = "ecs_cluster"
}

variable "ecs_service_name" {
  type = string
}

variable "min_capacity" {
  type    = number
  default = 1
}

variable "max_capacity" {
  type    = number
  default = 1
}

variable "ecs_policy_configurations_predefined" {
  type = list(
    object({
      predefined_metric_type = string
      target_value           = number
      scale_in_cooldown      = number
      scale_out_cooldown     = number
    })
  )
  default = []
}