resource "aws_ecs_cluster" "ecs_cluster" {
  name = var.ecs_cluster_name
}

resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = var.max_capacity
  min_capacity       = var.min_capacity
  resource_id        = "service/${aws_ecs_cluster.ecs_cluster.name}/${var.ecs_service_name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_policy_predefined" {
  count              = length(var.ecs_policy_configurations_predefined)
  name               = "ecs_policy${count.index}"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = var.ecs_policy_configurations_predefined[count.index].predefined_metric_type
    }

    target_value       = var.ecs_policy_configurations_predefined[count.index].target_value
    scale_in_cooldown  = var.ecs_policy_configurations_predefined[count.index].scale_in_cooldown
    scale_out_cooldown = var.ecs_policy_configurations_predefined[count.index].scale_out_cooldown
  }
}