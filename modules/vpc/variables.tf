variable "vpc_cidr_block" {
  type = string
}

variable "vpc_name" {
  type    = string
  default = "vpc"
}

variable "vpc_enable_dns_hostnames" {
  type    = bool
  default = false
}

variable "availability_zones" {
  type = list(string)
}

variable "public_ip_cidrs" {
  type = list(string)
}

variable "private_ip_cidrs" {
  type = list(string)
}

variable "nat_gateway_name" {
  type    = string
  default = "nat_gateway"
}

variable "internet_gateway_name" {
  type    = string
  default = "internet_gateway"
}

variable "private_route_table_name" {
  type    = string
  default = "private_route_table"
}

variable "public_route_table_name" {
  type    = string
  default = "public_route_table"
}

variable "additional_tags_public_subnet" {
  type    = map(string)
  default = null
}

variable "additional_tags_private_subnet" {
  type    = map(string)
  default = null
}