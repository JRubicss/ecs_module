variable "vpc_id" {
  type = string
}

variable "http_cidr_block" {
  type    = list(string)
  default = []
}

variable "egress_cidr_blocks" {
  type    = list(string)
  default = ["0.0.0.0/0"]
}

variable "security_group_name" {
  type    = string
  default = "alb_security_group"
}

variable "alb_name" {
  type    = string
  default = "alb"
}

variable "subnet_ids" {
  type = list(string)
}

variable "target_group_name_prefix" {
  type    = string
  default = "albtg"
}

variable "target_type" {
  type    = string
  default = "instance"
}

variable "alb_port" {
  type    = number
  default = 80
}

variable "application_port" {
  type    = number
  default = 80
}

variable "alb_protocol" {
  type    = string
  default = "HTTP"
}

variable "ec2_ids" {
  type    = list(string)
  default = []
}