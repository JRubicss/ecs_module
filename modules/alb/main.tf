resource "aws_security_group" "alb_security_group" {
  vpc_id = var.vpc_id

  ingress {
    from_port   = var.alb_port
    to_port     = var.alb_port
    protocol    = "tcp"
    cidr_blocks = var.http_cidr_block
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.egress_cidr_blocks
  }

  tags = {
    Name = var.security_group_name
  }
}

resource "aws_lb" "alb" {
  name               = var.alb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_security_group.id]
  subnets            = var.subnet_ids
}

resource "aws_lb_target_group" "alb_target_group" {
  name_prefix = var.target_group_name_prefix
  port        = var.application_port
  protocol    = var.alb_protocol
  vpc_id      = var.vpc_id
  target_type = var.target_type

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_target_group_attachment" "alb_target_group_attachment" {
  count            = length(var.ec2_ids)
  target_group_arn = aws_lb_target_group.alb_target_group.arn
  target_id        = var.ec2_ids[count.index]
  port             = var.alb_port
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = var.alb_port
  protocol          = var.alb_protocol

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_target_group.arn
  }
}