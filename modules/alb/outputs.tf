output "alb_security_group" {
  value = aws_security_group.alb_security_group
}

output "alb" {
  value = aws_lb.alb
}

output "target_group" {
  value = aws_lb_target_group.alb_target_group
}